express = require 'express'
fs = require 'fs'
path = require 'path'
cons = require 'consolidate'

module.exports = (app)->
	class Module
		constructor: (@basedir)->
			_.extend(@,express())
			name = @basedir.split('/').pop()
			@name = name
			@engine('html', cons.swig);
			@set 'views', path.join(@basedir, 'views')
			@set 'view engine', 'html'

			try
				@models = _.requireDir @basedir + '/models'
			catch e

			for type in ['images', 'fonts', 'stylesheets', 'javascripts']
				@use '/static/'+type+'/'+name, express.static path.join(@basedir, 'static', type)

			try
				modules = fs.readdirSync @basedir + '/modules'
				for name in modules
					$CMS.inject name, require(@basedir + '/modules/' + name + '/' + name)
				console.log modules
			catch e

			@use '/ng/'+name, express.static path.join(@basedir, 'ng')
			@use '/ng/'+name+'/views', (req, res, next)->
				view = req.url.replace(/\.jade|\.html|\.ejs/i, '').slice(1)
				res.render(view)

			return @
		_init: ()->
			#console.log @
			_.log 'info', 'Loading Module - ' + @name
			if @init
				return @init()
		_attach: (app)->
			@mothership = app
			self = @
			@mothership.ee.on 'init', ()->
				#console.log self
				self._init.apply self, arguments
			return
		_detach: ()->
			if @detach
				return @detach()
			return
		_uninstall: ()->
			if @uninstall
				return @uninstall()
			return

	return (basedir)->
		return new Module basedir
###
	return (dir)->
		module = express()

		module.set 'name', name

		module.set 'views', path.join(dir, 'views')
		module.set 'view engine', 'jade'

		module._init = ()->
			module.init()
		module._attach = ()->
			module.attach()
		module._detach = ()->
			module.detach()
		module._uninstall = ()->
			module.uninstall()

		return module
###