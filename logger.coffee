winston = require 'winston'
path = require 'path'

module.exports = (config)->
	logger = new winston.Logger
		transports: [
			new winston.transports.Console
				colorize: true
				timestamp: ()->
					pad = (str, padding)->
						padding = padding || "00"
						return padding.substr(0, padding.length - str.length) + str
					padIt = (number, index, list)->
						number = (number+'')
						list[index] = pad(number, "00")
					now = new Date()
					return _.each([now.getHours(),now.getMinutes(),now.getSeconds()], padIt).join(':') + '.' + pad(now.getMilliseconds()+'',"000")
			new winston.transports.File
				filename: path.join config.base, config.logger.file

		]
