_ = require 'lodash'

mixins =
	log: ()->
		$CMS.logger.log.apply($CMS.logger, arguments)
	Q: require 'q'
	requireDir: require('require-dir')

_.mixin mixins

module.exports = _