express = require 'express'
http = require 'http'
GLOBAL._ = require './util'
EventEmitter2 = require('eventemitter2').EventEmitter2;

class Application
	constructor: (basedir)->
		GLOBAL.$CMS = @
		@config = require(basedir + '/config')
		@config.base = basedir
		@_modules = {}

		@expressApp = express()
		@mongoose = require('mongoose-q')()
		@app = @expressApp
		@logger = require('./logger')(@config)
		@ee = new EventEmitter2
			wildcard: true
			delimiter: '::'
			newListener: true
			maxListeners: 20
		@Model = require('./model')(@)
		@Module = require('./module')(@)
		return @
	inject: (name, module)->
		@_modules[name] = module
		@_modules[name]._attach(@)
		@app.use @_modules[name]
	use: (name)->
		shortname = name.split('/')[1]
		@inject name, require(@config.base + '/modules/' + name + '/' + name)
	init: ()-> # Load all configuration and modules
		console.log(@config.modules)
		for name in @config.modules
			@use(name)
		@ee.emit('init')
		return
	start: ()-> # Start listening on server
		@server = http.createServer(@expressApp);
		@server.listen(@config.server.port)
		_.log('info', 'Server listening on port '+@config.server.port)
		return



module.exports = (dir)->
	app = new Application(dir)
	return app


